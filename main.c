#include <msp430.h> 
#include "inc/bibliotheek.h"

#pragma vector = PORT1_VECTOR
__interrupt void ISR_poort1 (void)
{
    P2OUT ^= 1<<2;
    if ((P1IFG & 1<<0) != 0)
    {
     output_pin(2, 2, hoog);
     P1IFG &= ~(1<<0);
    }
    if ((P1IFG & 1<<1) != 0)
    {
        output_pin(2, 2, laag);
        P1IFG &= ~(1<<1);
    }
}

/**
 * main.c
 */
int main(void)
{
    zet_klok_op_MHz(1);
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	zet_pin_richting(2, 0, output);
	zet_pin_richting(2, 1, output);
	zet_pin_richting(2, 2, output);
	output_pin(2, 2, laag);

	zet_pin_richting(1, 0, input);
	zet_interne_weerstand(1, 0, pull_down);
	zet_pin_richting(1, 1, input);
	zet_interne_weerstand(1, 1, pull_up);

	P1IE |= (1<<0);
	P1IES &= ~(1<<0) ;
	P1IFG &= ~(1<<0);
	P1IE |= (1<<1);
	P1IES &= ~(1<<1);
	P1IFG &= ~(1<<1);
	__enable_interrupt();


	while(1)
	{
	  __delay_cycles(2000000);
	  output_pin(2, 0, hoog);
	  __delay_cycles(2000000);
	  output_pin(2, 0, laag);
	}
	
	return 0;
}
